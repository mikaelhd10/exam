package demo;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import code.*;

// On veut tester:
// le calcul de la reussite/echec

class tests {

	// Un assert par valeur est mieux que 5 fonctions ou encore 5 conditions dans un assert ¯\_(ツ)_/¯
	@Test // On s'assure que l'algo de tri fonctionne dans le pire cas possible
	void testAlgoTriPireCas() {
		//setup
		double[] args = {100.,80.,55.,30.,10.};
		Notes notes_test = new Notes(args);
		notes_test.trierNotes();
		
		assertTrue(notes_test.getNotes()[0].getResultat() == 10.);
		assertTrue(notes_test.getNotes()[1].getResultat() == 30.);
		assertTrue(notes_test.getNotes()[2].getResultat() == 55.);
		assertTrue(notes_test.getNotes()[3].getResultat() == 80.);
		assertTrue(notes_test.getNotes()[4].getResultat() == 100.);
	}

	@Test // On s'assure que l'algo de tri fonctionne lorsque deux valeurs dupliquees
	void testAlgoTriMemeValeurs() {
		//setup
		double[] args = {100.,80.,80.,30.,30.};
		Notes notes_test = new Notes(args);
		notes_test.trierNotes();
		
		assertTrue(notes_test.getNotes()[0].getResultat() == 30.);
		assertTrue(notes_test.getNotes()[1].getResultat() == 30.);
		assertTrue(notes_test.getNotes()[2].getResultat() == 80.);
		assertTrue(notes_test.getNotes()[3].getResultat() == 80.);
		assertTrue(notes_test.getNotes()[4].getResultat() == 100.);
	}
	
	@Test // On teste que les ponderations sont bien appliquees
	void testPonderations() {
		//setup (on se fout des valeurs)
		double[] args = {100.,80.,80.,30.,30.};
		Notes notes_test = new Notes(args);
		
		assertTrue(notes_test.getNotes()[0].getPonderation() == Notes.PONDERATION_PIRES_NOTES);
		assertTrue(notes_test.getNotes()[1].getPonderation() == Notes.PONDERATION_PIRES_NOTES);
		assertTrue(notes_test.getNotes()[2].getPonderation() == Notes.PONDERATION_TROISIEME_NOTE);
		assertTrue(notes_test.getNotes()[3].getPonderation() == Notes.PONDERATION_DEUXIEME_NOTE);
		assertTrue(notes_test.getNotes()[4].getPonderation() == Notes.PONDERATION_PREMIERE_NOTE);
	}
	
	@Test // On teste que la moyenne est bien calculee
	void testMoyenne() {
		//setup
		double[] args = {10.,10.,65.,50.,55.};
		Notes notes_test = new Notes(args);
		notes_test.trierNotes();
		notes_test.calculerMoyenne();
		
		// 57.75 est la moyenne ponderee des 3 meilleurs resultats
		assertTrue(notes_test.getMoyenne() == 57.75);
	}
	
	@Test
	void testReussite() {
		//setup
		double[] args = {10.,10.,80.,50.,55.};
		Notes notes_test = new Notes(args);
		notes_test.trierNotes();
		notes_test.calculerMoyenne();
		
		assertTrue(notes_test.calculerReussite());
	}
	
	@Test
	void testEchec() {
		//setup
		double[] args = {10.,10.,65.,50.,55.};
		Notes notes_test = new Notes(args);
		notes_test.trierNotes();
		notes_test.calculerMoyenne();
		
		assertFalse(notes_test.calculerReussite());
	}

}
