package demo;

public class Main {

	public static void main(String[] args) {
		// On cree un array des resultats
		double[] resultats = new double[5];
		for (int i=0; i < args.length; i++) {
			resultats[i] = Double.parseDouble(args[i]);
		}
		
		// On cree l'objet Notes et on fait les calculs
		Notes notes = new Notes(resultats);
		notes.trierNotes();
		notes.calculerMoyenne();
		
		if (notes.calculerReussite()) {
			System.out.println("Reussite");
		}
		else {
			System.out.println("Echec");
		}
	}

}
