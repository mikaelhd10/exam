package demo;

public class Note {
	private double resultat;
	private double ponderation; // entre 0 et 1
	
	
	public Note (double resultat) {
		this.resultat = resultat;
	}
	
	public void setPonderation (double ponderation) {
		this.ponderation = ponderation;
	}
	
	public void setResultat(double resultat) {
		this.resultat = resultat;
	}
	
	public double getResultat() {
		return this.resultat;
	}
	
	public double getPonderation() {
		return ponderation;
	}
}
