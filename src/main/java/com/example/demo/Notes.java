package demo;

public class Notes {
	private Note[] notes = new Note[5];
	private double moyenne;
	public static final double QUANTITE_NOTES_NON_CALCULEES = 2;
	public static final double PONDERATION_PIRES_NOTES = 0;
	public static final double PONDERATION_PREMIERE_NOTE = 0.40;
	public static final double PONDERATION_DEUXIEME_NOTE = 0.35;
	public static final double PONDERATION_TROISIEME_NOTE = 0.25;
	
	public Notes(double[] resultats) {
		for (int i = 0; i < resultats.length; i++) {
			notes[i] = new Note(resultats[i]);
		}
		moyenne = 0;
		for (int i = 0; i < QUANTITE_NOTES_NON_CALCULEES; i++) {
			notes[i].setPonderation(PONDERATION_PIRES_NOTES);
		}
		notes[2].setPonderation(PONDERATION_TROISIEME_NOTE);
		notes[3].setPonderation(PONDERATION_DEUXIEME_NOTE);
		notes[4].setPonderation(PONDERATION_PREMIERE_NOTE);
	}

	// pas le meilleur algo de tri, my bad (il fait O(n^2), woooops)
	// trie les notes en ordre croissant
	// J'aurais pu faire swap les objets Note au lieu de changer les valeurs, mais non
	public void trierNotes() {
		for (int i = 0; i < notes.length - 1; i++) {
			int index = i;
			for (int j = i + 1; j < notes.length; j++) {
				if (notes[j].getResultat() < notes[index].getResultat()) {
					index = j;
				}
			}
			double min = notes[index].getResultat();
			notes[index].setResultat(notes[i].getResultat());
			notes[i].setResultat(min);
		}
	}

	// Ici, on assume que les notes sont tri�es en ordre croissant
	public void calculerMoyenne() {
		moyenne = 0;
		for (int i = 0; i < notes.length; i++) {
			moyenne += notes[i].getResultat() * notes[i].getPonderation();
		}
	}
	
	public Boolean calculerReussite() {
		if(moyenne >= 60) {
			return true;
		}
		return false;
	}
	
	public Note[] getNotes() {
		return notes;
	}
	
	public double getMoyenne() {
		return moyenne;
	}
}
